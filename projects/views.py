from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required


# Create your views here.
from .models import Project
from .forms import ProjectForm


@login_required
def list_projects(request):
    projects = Project.objects.filter(owner=request.user)
    context = {"projects": projects}
    return render(request, "projects/list_projects.html", context)


@login_required
def project_details(request, id):
    details = Project.objects.get(id=id)
    context = {"show_project": details}
    return render(request, "projects/details.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            newproj = form.save(False)
            newproj.owner = request.user
            newproj.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()
    context = {"form": form}
    return render(request, "projects/create.html", context)
