from django.shortcuts import render, redirect

# Create your views here.
from .forms import LoginForm, RegisterForm
from django.contrib.auth.models import User
from django.contrib.auth import login, authenticate, logout


# ACCOUNT LOGIN
def acc_login(request):
    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect("home")
            form.add_error(None, "Invalid credentials.")
        form = LoginForm()
    form = LoginForm()
    context = {"form": form}
    return render(request, "accounts/login.html", context)


# ACCOUNT LOGOUT
def acc_logout(request):
    logout(request)
    return redirect("login")


# ACCOUNT REGISTRATION
def acc_signup(request):
    if request.method == "POST":
        form = RegisterForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            password_confirmation = form.cleaned_data["password_confirmation"]
            if password == password_confirmation:
                user = User.objects.create_user(
                    username,
                    password=password,
                )
                login(request, user)
                return redirect("list_projects")
            form.add_error("the passwords do not match")
    form = RegisterForm()
    context = {"form": form}
    return render(request, "accounts/signup.html", context)
