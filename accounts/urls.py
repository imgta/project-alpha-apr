from django.urls import path
from .views import acc_login, acc_logout, acc_signup

urlpatterns = [
    path("login/", acc_login, name="login"),
    path("logout/", acc_logout, name="logout"),
    path("signup/", acc_signup, name="signup"),
]
