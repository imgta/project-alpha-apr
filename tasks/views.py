from django.shortcuts import render, redirect

# Create your views here.
from .models import Task
from .forms import TaskForm
from django.contrib.auth.decorators import login_required


@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            newtask = form.save(False)
            newtask.save()
            return redirect("list_projects")
    form = TaskForm()
    context = {"form": form}
    return render(request, "tasks/create_task.html", context)


@login_required
def my_tasks(request):
    mytask = Task.objects.filter(assignee=request.user)
    context = {"tasks": mytask}
    return render(request, "tasks/mylist.html", context)
